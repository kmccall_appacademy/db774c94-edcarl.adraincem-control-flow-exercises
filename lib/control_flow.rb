# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  uppercase_str = ""
  str.each_char { |ch| uppercase_str << ch if ch == ch.upcase }
  uppercase_str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  str_mid = str.length / 2
  if str.length.odd?
    str[str_mid]
  else
    str[str_mid - 1] + str[str_mid]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char do |ch|
    count += 1 if VOWELS.include?(ch)
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  num.downto(1) { |number| product *= number }
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined = ""
  arr.each_with_index do |el, idx|
    if idx != arr.length - 1
      joined << "#{el}#{separator}"
    else
      joined << el.to_s
    end
  end
  joined
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_str = ""
  str.each_char.with_index do |ch, idx|
    if (idx + 1).even?
      weird_str << ch.upcase
    else
      weird_str << ch.downcase
    end
  end
  weird_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  new_str = []
  str.split.each do |word|
    if word.length >= 5
      new_str << word.reverse
    else
      new_str << word
    end
  end
  new_str.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr_of_int = []
  (1..n).each do |num|
    if num % 3 == 0 && num % 5 == 0
      arr_of_int << "fizzbuzz"
    elsif num % 3 == 0
      arr_of_int << "fizz"
    elsif num % 5 == 0
      arr_of_int << "buzz"
    else
      arr_of_int << num
    end
  end
  arr_of_int
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr_reverse = []
  idx = arr.length - 1
  while idx >= 0
    arr_reverse << arr[idx]
    idx -= 1
  end
  arr_reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  num.times { |count| return false if count > 1 && num % count == 0 }
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  sorted_arr = []
  (1..num).each do |count|
    sorted_arr << count if num % count == 0
  end
  sorted_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  sorted_arr = []
  factors_arr = factors(num)
  factors_arr.each { |el| sorted_arr << el if prime?(el) }
  sorted_arr
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even_num = arr.select(&:even?)
  odd_num = arr.select(&:odd?)

  if even_num.length < odd_num.length
    even_num[0]
  else
    odd_num[0]
  end
end
